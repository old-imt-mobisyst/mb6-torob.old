#include <iostream>

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>

#include "tools/float2.h"

using namespace std;

// global element :
tf::Vector3 _goal;
std::string _goal_frame_id;
std::string _cmd_frame_id;

ros::Publisher _cmd_publisher;

tf::TransformListener * _listener;

double _dmax_l_speed, _max_a_speed;
float _epsilon;

// Callback :
void goal_subscriber(const geometry_msgs::PoseStamped & g);
void move();

bool transformationFromTo( tf::StampedTransform * transform, std::string from_frame_id, std::string to_frame_id );
tf::Vector3 transformFromTo(tf::Vector3 target, std::string from_frame_id, std::string to_frame_id );

void print(const tf::Vector3 &v){
  std::cout << v.x() << ", " << v.y() << ", " << v.z() ;
}

int main(int argc, char **argv)
{
    // ROS:
    cout << "Initialize Torobo::Move: " << endl;
    ros::init( argc, argv, "move" );
    ros::NodeHandle node;
    ros::NodeHandle node_private("~");

    _listener= new tf::TransformListener(node);

    // Configuration:
    std::string goal_topic, cmd_topic;
    std::string init_goal_frame_id;
    float goal_x, goal_y;

    if( !node_private.getParam("cmd_topic", cmd_topic) ) cmd_topic= "/cmd_vel_mux/input/navi";
    if( !node_private.getParam("cmd_frame_id", _cmd_frame_id) ) _cmd_frame_id= "base_link";

    if( !node_private.getParam("linear_speed", _dmax_l_speed) ) _dmax_l_speed= 0.2;
    if( !node_private.getParam("angular_speed", _max_a_speed) ) _max_a_speed= 1.2;
    if( !node_private.getParam("epsilon", _epsilon) ) _epsilon= 0.1f; // control position accepted erro in meters

    if( !node_private.getParam("init_goal_x", goal_x) ) goal_x= 0.f;
    if( !node_private.getParam("init_goal_y", goal_y) ) goal_y= 0.f;
    if( !node_private.getParam("init_goal_frame_id", _goal_frame_id) ) init_goal_frame_id= _cmd_frame_id;

    if( !node_private.getParam("goal_topic", goal_topic) ) goal_topic= "move_base_simple/goal";
    if( !node_private.getParam("goal_frame_id", _goal_frame_id) ) _goal_frame_id= "odom";

    _goal.setX( goal_x );
    _goal.setY( goal_y );
    _goal.setZ( 0.0f );

    _goal= transformFromTo( _goal, init_goal_frame_id, _goal_frame_id );

    cout << "initial goal: ";
    print( _goal );
    cout << " in "<< _goal_frame_id;
    cout << "\nframe "<< _cmd_frame_id << " :";
    print( transformFromTo( tf::Vector3(0.f, 0.f, 0.f), _cmd_frame_id, _goal_frame_id ) );
    cout << endl;

    // subscribers function:
    ros::Subscriber sub2 = node.subscribe( goal_topic, 1, goal_subscriber );

    // publisher function:
    _cmd_publisher= node.advertise<geometry_msgs::Twist>( cmd_topic, 1 );

    // Manualy spin ROS:
    ros::Rate loop_rate(10);
    while( ros::ok() ){
      move();
      ros::spinOnce();
      loop_rate.sleep();
    }

//    // get the hand to ros:
//    cout << "run torob move" << endl;
//    ros::spin();

    // Properly stop the program:
    cout << "close move" << endl;

    delete _listener;

    return 0;
}

void goal_subscriber(const geometry_msgs::PoseStamped & g){

  cout << "goal_subscriber" << endl;

  _goal.setX( g.pose.position.x );
  _goal.setY( g.pose.position.y );
  _goal.setZ( g.pose.position.z );

  cout << "goal in " << g.header.frame_id  << " frame :" << _goal.x() << ", " << _goal.y() << endl;

  if ( _listener->waitForTransform( _goal_frame_id, g.header.frame_id, g.header.stamp, ros::Duration(0.5) ) )
  {
    tf::StampedTransform toRefGoalFrame;

    try{
      _listener->lookupTransform( _goal_frame_id, g.header.frame_id, g.header.stamp,  toRefGoalFrame );
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
    }

    _goal= toRefGoalFrame * _goal;

    cout << "goal in " << _goal_frame_id  << " frame :" << _goal.x() << ", " << _goal.y() << endl;
  }
  else{
    cerr << "Transform " <<  g.header.frame_id << "-" << _goal_frame_id << " unvailable" <<  endl;
  }
}

bool transformationFromTo( tf::StampedTransform * transform, std::string from_frame_id, std::string to_frame_id ){

  if ( from_frame_id.compare( to_frame_id ) == 0 )
    return false;

  bool transform_ok= true;
  ros::Time now= ros::Time(0);

  // Wait for available transformation :
  if( !_listener->waitForTransform( to_frame_id, from_frame_id, now, ros::Duration(0.5) ) ){
    cerr << "transform from: " << from_frame_id
         << " to: " << to_frame_id << " unvailable." << endl;
    transform_ok= false;
  }

  // Get Transforms (goal -> scan frames and scan -> commande frames):
  try{
    _listener->lookupTransform( to_frame_id, from_frame_id,
                                now, *transform );
  }
  catch (tf::TransformException ex){
    ROS_ERROR("%s",ex.what());
    transform_ok= false;
  }

  return transform_ok;
}

tf::Vector3 transformFromTo(tf::Vector3 target, std::string from_frame_id, std::string to_frame_id ){

  if ( from_frame_id.compare( to_frame_id ) == 0 )
    return target;

  // Get Transforms (goal -> scan frames and scan -> commande frames):
  tf::StampedTransform transform;

  // If transforms exist move goal in scan frame:
  if( transformationFromTo(&transform, from_frame_id, to_frame_id) )
  {
    return transform * target;
  }

  return tf::Vector3(0.f, 0.f, 0.f);
}

void move(){
  cout << "move (" << _goal_frame_id << ", " << _cmd_frame_id << ")";
  cout << "\n-> goal :";
  print( _goal );
  cout << "\n-> "<< _cmd_frame_id << " :";
  print( transformFromTo( tf::Vector3(0.f, 0.f, 0.f), _cmd_frame_id, _goal_frame_id ) );

  // transform goal position from goal frame to cmd frame :
  tf::Vector3 localGoal= transformFromTo(_goal, _goal_frame_id, _cmd_frame_id );
  cout << "\n-> local goal :";
  print( localGoal );
  cout << std::endl;

  // generate appropriate commande message :
  mia::Float2 norm_goal(localGoal.x(), localGoal.y());
  float d= norm_goal.normalize();

  geometry_msgs::Twist cmd;

  cmd.linear.x = 0.0;
  cmd.linear.y = 0.0;
  cmd.linear.z = 0.0;

  cmd.angular.x = 0.0;
  cmd.angular.y = 0.0;
  cmd.angular.z = 0.0;

  cout << "-> generate the command: distance "
    << d  << " vs " << _epsilon << endl;

  if( d > _epsilon )// !stop condition :
  {
    if( localGoal.x() > _epsilon )
    {
      cmd.linear.x= _dmax_l_speed + min( 0.5*localGoal.x()*_dmax_l_speed, _dmax_l_speed );

      if( 0.005f < norm_goal.y )
        cmd.angular.z= min( norm_goal.y*2.0*_max_a_speed, _max_a_speed );

      if( -0.005f > norm_goal.y )
        cmd.angular.z= -(min( norm_goal.y*-2.0*_max_a_speed, _max_a_speed ));
    }
    else
    {
      cmd.angular.z= _max_a_speed;
    }
  }

  cout << "\tcommande linear: " << cmd.linear.x << ", angular: " << cmd.angular.z << endl;

  _cmd_publisher.publish( cmd );

  cout << "-> end generate the command: " << endl;
}
