#include <iostream>

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>

#include "tools/float2.h"

using namespace std;

// global element :
tf::Vector3 _goal;
std::string _goal_frame_id;
int _loop_rate= 10;

ros::Publisher _local_goal_publisher;
//ros::Publisher _markers_publisher; ?

tf::TransformListener * _listener;

// Callback :
void goal_subscriber(const geometry_msgs::PoseStamped & g);
void local_goal();

bool transformationFromTo( tf::StampedTransform * transform, std::string from_frame_id, std::string to_frame_id );
tf::Vector3 transformFromTo(tf::Vector3 target, std::string from_frame_id, std::string to_frame_id );

void print(const tf::Vector3 &v){
  std::cout << v.x() << ", " << v.y() << ", " << v.z() ;
}

int main(int argc, char **argv)
{
    // ROS:
    cout << "Initialize Torobo::Ring-State: " << endl;
    ros::init( argc, argv, "move" );
    ros::NodeHandle node;
    ros::NodeHandle node_private("~");

    _listener= new tf::TransformListener(node);

    // Configuration:
    std::string goal_topic, local_goal_topic;
    float goal_x, goal_y;

    if( !node_private.getParam("local_goal_topic", local_goal_topic) ) local_goal_topic= "/local_goal";

    if( !node_private.getParam("goal_topic", goal_topic) ) goal_topic= "move_base_simple/goal";

    if( !node_private.getParam("goal_frame_id", _goal_frame_id) ) _goal_frame_id= "odom";
    if( !node_private.getParam("init_goal_x", goal_x) ) goal_x= 0.f;
    if( !node_private.getParam("init_goal_y", goal_y) ) goal_y= 0.f;

    _goal.setX( goal_x );
    _goal.setY( goal_y );
    _goal.setZ( 0.0f );

    cout << "initial goal: ";
    print( _goal );
    cout << " in "<< _goal_frame_id;
    cout << endl;

    // subscribers function:
    ros::Subscriber sub2 = node.subscribe( goal_topic, 1, goal_subscriber );

    // publisher function:
    _local_goal_publisher= node.advertise<geometry_msgs::PoseStamped>( local_goal_topic, 1 );

    // Manualy spin ROS:
    ros::Rate loop_rate(_loop_rate);
    while( ros::ok() ){
      local_goal();
      ros::spinOnce();
      loop_rate.sleep();
    }

//    // get the hand to ros:
//    cout << "run torob move" << endl;
//    ros::spin();

    // Properly stop the program:
    cout << "close move" << endl;

    delete _listener;

    return 0;
}

void goal_subscriber(const geometry_msgs::PoseStamped & g){

  cout << "goal_subscriber" << endl;

  _goal.setX( g.pose.position.x );
  _goal.setY( g.pose.position.y );
  _goal.setZ( g.pose.position.z );

  cout << "goal in " << g.header.frame_id  << " frame :" << _goal.x() << ", " << _goal.y() << endl;

  if ( _listener->waitForTransform( _goal_frame_id, g.header.frame_id, g.header.stamp, ros::Duration(0.5) ) )
  {
    tf::StampedTransform toRefGoalFrame;

    try{
      _listener->lookupTransform( _goal_frame_id, g.header.frame_id, g.header.stamp,  toRefGoalFrame );
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
    }

    _goal= toRefGoalFrame * _goal;

    cout << "goal in " << _goal_frame_id  << " frame :" << _goal.x() << ", " << _goal.y() << endl;
  }
  else{
    cerr << "Transform " <<  g.header.frame_id << "-" << _goal_frame_id << " unvailable" <<  endl;
  }
}

bool transformationFromTo( tf::StampedTransform * transform, std::string from_frame_id, std::string to_frame_id ){

  if ( from_frame_id.compare( to_frame_id ) == 0 )
    return false;

  bool transform_ok= true;
  ros::Time now= ros::Time(0);

  // Wait for available transformation :
  if( !_listener->waitForTransform( to_frame_id, from_frame_id, now, ros::Duration(0.5) ) ){
    cerr << "transform from: " << from_frame_id
         << " to: " << to_frame_id << " unvailable." << endl;
    transform_ok= false;
  }

  // Get Transforms (goal -> scan frames and scan -> commande frames):
  try{
    _listener->lookupTransform( to_frame_id, from_frame_id,
                                now, *transform );
  }
  catch (tf::TransformException ex){
    ROS_ERROR("%s",ex.what());
    transform_ok= false;
  }

  return transform_ok;
}

tf::Vector3 transformFromTo(tf::Vector3 target, std::string from_frame_id, std::string to_frame_id ){

  if ( from_frame_id.compare( to_frame_id ) == 0 )
    return target;

  // Get Transforms (goal -> scan frames and scan -> commande frames):
  tf::StampedTransform transform;

  // If transforms exist move goal in scan frame:
  if( transformationFromTo(&transform, from_frame_id, to_frame_id) )
  {
    return transform * target;
  }

  return tf::Vector3(0.f, 0.f, 0.f);
}

void local_goal(){
  // TODO....
}
