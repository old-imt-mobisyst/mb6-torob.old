TOpological ROBot ROS package
=============================

TOROB is a ROS package dedicated for hight level topological abstraction of the enviroenement.

Control architecture based on local topological model and vector based maps.

## License GNU LGPL version 3

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

## Compile it for the first time in local directory :

```
bin/initialize
bin/make
source bin/setup.bash
```

## ROS nodes:

### move